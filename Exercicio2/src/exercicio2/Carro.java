/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercicio2;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lwyz_
 */
public class Carro implements Runnable{

    private int tempoVolta;
    private String nome;
    private final static Random generator = new Random();
    private int tempoCorrida;

    public Carro(String nome) {
        this.nome = nome;
        this.tempoCorrida = 0;
    }
    
    @Override
    public void run() {
        try {

            this.tempoVolta = generator.nextInt(1000); // Tempo da volta do carro
            Thread.sleep(tempoVolta);
            this.tempoCorrida = this.tempoCorrida + this.tempoVolta;

        } catch (InterruptedException ex) {
            System.out.println("O carro " + this.nome + " quebrou!");
        }
    }

    public int getTempoCorrida() {
        return tempoCorrida;
    }

    public String getNome() {
        return nome;
    }
    
}
