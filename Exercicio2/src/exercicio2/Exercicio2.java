/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercicio2;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Luiz
 */
public class Exercicio2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        System.out.println("Inicio da corrida.");
        
        ExecutorService executor = Executors.newCachedThreadPool();
        
        System.out.println("Preparando carros");
        Carro carros[] = new Carro[4];
        carros[0] = new Carro("carro 1");
        carros[1] = new Carro("carro 2");
        carros[2] = new Carro("carro 3");
        carros[3] = new Carro("carro 4");
        
        System.out.println("Largada");
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < carros.length; j++) {
                executor.execute(new Thread(carros[j]));
            }
        }

        System.out.println("Fim da corrida");

        executor.shutdown();
        try {
                executor.awaitTermination(1, TimeUnit.DAYS);
            } catch (InterruptedException e) {
                e.printStackTrace();
        }

        System.out.println("Tempo dos carros");
        
        for (int j = 0; j < carros.length; j++) {
            System.out.println(carros[j].getNome() + " tempo " + carros[j].getTempoCorrida());
        }
       
        String carro = "";
        int tempo = 0;
        for (int j = 0; j < carros.length; j++) {
            if (tempo == 0 || carros[j].getTempoCorrida() < tempo) {
                carro = carros[j].getNome();
                tempo = carros[j].getTempoCorrida();
            }
        }
        System.out.println("Vencedor " + carro + " tempo: "+tempo);
    }
    
}
